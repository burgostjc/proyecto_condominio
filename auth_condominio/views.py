from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import CreateUserForm, PerfilForm
from django.contrib.auth.decorators import permission_required

from django.contrib.auth.models import User

from django.contrib import messages

# Create your views here.

def loginView(request):

    template_name = 'auth_condominio/login.html'
    data = {}

    logout(request)
    username = password = ''
    if request.POST:
        user = authenticate(
                username=request.POST['username'],
                password=request.POST['password']
            )

        print(username)
        #print(password)
        if user is not None:
            if user.is_active:
                login(request, user)

                return redirect('condominio:home')
            else:
                print("usuario o pass incorrecta")
                messages.warning(
                    request,
                    'Usuario o contraseña incorrecto!'
                )

        else:
            print("no encuentra user")
            messages.error(
                request,
                'Usuario o contraseña incorrecto'
            )
    return render(request, template_name, data)


def logoutView(request):
    logout(request)
    return redirect('auth_condominio:login')

@permission_required('condominio.add_perfil')
def registerView(request):
    template_name = 'auth_condominio/register.html'
    data = {}

    if request.method == 'POST':
        user_form = CreateUserForm(request.POST)
        perfil_form = PerfilForm(request.POST)
        if user_form.is_valid() and perfil_form.is_valid():
            uf = user_form.save(commit=False)
            pf = perfil_form.save(commit=False)

            pf.user = uf
            uf.save()
            pf.save()
            user_form.save_m2m()
            # group = Group.objects.get(name='customer')
            # user.groups.add(group)
            return redirect('condominio:usuarios')

        elif User.objects.filter(username=request.POST['username']).exists():
            messages.warning(
                request,
                'Este nombre usuario ya está en uso'
            )
        else:
            messages.warning(
                request,
                'Registro no válido'
            )
        # print(request.POST['username'])
        # print(request.POST['first_name'])
        # print(request.POST['email'])
        # print(request.POST['password1'])
        # print(request.POST['password2'])
        # print(request.POST['rut'])
    else:
        user_form = CreateUserForm()
        perfil_form = PerfilForm()
        # messages.warning(
        #     request,
        #     'Registro no válido2'
        # )

    data = {
        'user_form': user_form,
        'perfil_form': perfil_form

    }

    return render(request, template_name, data)

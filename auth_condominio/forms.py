from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from condominio.models import Perfil


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username',
            'first_name',
            'last_name',
            'email',
            'groups',
            'password1',
            'password2',
        ]

class PerfilForm(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = ['rut']

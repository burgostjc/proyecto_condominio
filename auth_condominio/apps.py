from django.apps import AppConfig


class AuthCondominioConfig(AppConfig):
    name = 'auth_condominio'

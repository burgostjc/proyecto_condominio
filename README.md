# Proyecto Condominio Las Flores.

_Actualmente el Condominio Las Flores registra a sus visitas de manera manual, pidiendo los datos de cada persona que visita a algún propietario que resida en el condominio, anotándolos en un libro destinado para eso._

_Además, cada vez que la administración del recinto desea hacer anuncios a los propietarios, se limita a hacerlos de manera manual, teniendo que imprimir los avisos y entregándolos por medio de la conserjería._

_Debido a sus limitaciones en tecnología, se propone una aplicación web en la cual se podrán registrar todas las visitas que se realicen al recinto, registrando cada visitante que asiste al lugar. Y, por otro lado, los administradores, conserjes y propietarios del recinto podrán interactuar mediante anuncios que se realicen en la página._

## Comenzando

### Requisitos

Se debe tener instalado lo siguiente:

  * Docker
  * Docker Compose
  * Git

### Despliegue

_En primer lugar se debe clonar el proyecto en el computador.
Situados en la raiz de donde se quiere clonar el proyecto, abrimos la CMD y escribimos:_

```
git clone URL
```

_Debe considerar que URL es el link SSH o HTTPS para clonar el proyecto._

_Ya clonado el proyecto en su computador, se debe puede iniciar el proyecto escribiendo:_

```
docker-compose up -d
```

_Con esto ya tenemos el proyecto up en nuestro local host. Para entrar a la aplicación web, debemos abrir nuestro navegador y entrar al URL:_

```
localhost:8001
```

_Esta página es la página principal de la aplicación web._

_Para poder acceder a las funcionalidades que tiene la aplicación, se presionar Ingresar en el Menu de navegación._

_Cuando desee cerrar la aplicación web, debe asegurarse de detener el contenedor. Para esto escribiremos en el CMD (en la raiz del proyecto también):_

```
docker-compose stop
```

## Características de la aplicación web

_Debido a que la aplicación web es para habitantes, conserjes y administradores del condominio, y también tiene un home de presentación para la gente que visita el sitio, se ha restringido el registro de usuarios. Este sólo lo puede realizar un administrador del sistema, con el fin de evitar a usuarios que no pertenecen al condominio._

  * _Los usuarios **habitantes** del sistema podrán acceder al apartado de anuncios, crear un anuncio, editar, eliminar su anuncio y ver los demás anuncios creados por habitantes, conserjes y administradores, los cuales están separados por sección para poder tener mayor control de estos._

  * _Los usuarios **conserjes** del sistema podrán acceder a los anuncios y además podrán agregar *visitas* que se realizan al condominio. Para ello, si el visitante no se encuentra anteriormente registrado, lo deberá registrar en el apartado *visitante*. También podrán ver todos los departamentos del condominio, viendo qué habitantes viven en cada departamento._

  * _Los usuarios **administrador** tendrán acceso a todo el sistema, que además de lo anterior mencionado, también podrán ver los usuarios registrados en el sistema, editar su información o eliminar a un usuario. También podrá registrar a nuevos usuarios dentro del sistema._

  * _Todos los usuarios podrán ver su perfil y cambiar su contraseña, pero solo los administradores podrán cambiar los datos de estos, tales como nombre, apellido, departamento, entre otros._

### Usuarios y contraseñas registrados.

Usuario | Contraseña | Rol
------- | ---------- | ---
admin | 123 | administrador
primeradministrador | hola12hola | administrador
primerconserje | hola12hola | conserje
primerhabitante | hola12hola | habitante
conserjenuevo2 | hola12hola | conserje
usuario1 | hola12hola | habitante

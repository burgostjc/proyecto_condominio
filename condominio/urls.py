from django.urls import path

from . import views

app_name = 'condominio'

urlpatterns = [
    path('', views.home, name = 'home'),
    path('Visitantes', views.lista_visitante, name = 'lista_visitante'),
    path('Crear_Visitante', views.crear_visitante, name = 'crear_visitante'),
    path('Editar_Visitante/<int:visitante_id>', views.editar_visitante, name = 'editar_visitante'),
    path('Eliminar_Visitante/<int:visitante_id>', views.eliminar_visitante, name = 'eliminar_visitante'),
    path('Visitas', views.lista_visita, name = 'lista_visita'),
    path('Crear_Visita', views.crear_visita, name = 'crear_visita'),
    path('Editar_Visita/<int:visita_id>', views.editar_visita, name = 'editar_visita'),
    path('Eliminar_Visita/<int:visita_id>', views.eliminar_visita, name = 'eliminar_visita'),
    path('Departamentos', views.lista_departamento, name = 'lista_departamento'),
    path('Crear_Departamento', views.crear_departamento, name = 'crear_departamento'),
    path('Editar_Departamento/<int:departamento_id>', views.editar_departamento, name = 'editar_departamento'),
    path('Eliminar_Departamento/<int:departamento_id>', views.eliminar_departamento, name = 'eliminar_departamento'),

    path('Anuncios/', views.verAnunciosViews, name = 'anuncios'),
    path('Anuncios/crear/', views.crearAnunciosViews, name = 'crearAnun'),
    path('Anuncios/borrar/<int:anuncio_id>', views.borrarAnuncioViews, name = 'borrarAnun'),
    path('Anuncios/editar/<int:anuncio_id>', views.editarAnuncioViews, name = 'editarAnun'),

    path('Usuarios/perfiles/', views.verUsuariosViews, name = 'usuarios'),
    path('Usuarios/perfiles/editar/<int:perfil_id>', views.editarUsuariosViews, name = 'editarUser'),
    path('Usuarios/perfiles/borrar/<int:usuario_id>', views.borrarUsuarioViews, name = 'borrarUser'),

    path('Perfil/', views.verPerfilViews, name = 'perfil'),
    path('Perfil/Changepassword/', views.actualizarPassPerfil, name = 'perfilPass'),

]

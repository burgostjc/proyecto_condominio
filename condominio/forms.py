from django import forms
from condominio.models import Perfil, Anuncio, Departamento, Visitante, Visita
from django.contrib.auth.models import User

class VisitanteForm(forms.ModelForm):
    class Meta:
        model = Visitante
        fields = '__all__'

class VisitaForm(forms.ModelForm):
    class Meta:
        model = Visita
        fields = '__all__'

class DepartamentoForm(forms.ModelForm):
    class Meta:
        model = Departamento
        fields = '__all__'

class AnuncioForm(forms.ModelForm):
    class Meta:
        model = Anuncio
        fields = ['titulo',
            'descripcion',
            'foto_anuncio',
        ]

class PerfilForm(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = '__all__'

class UserUpdateForm(forms.ModelForm):
    # email = forms.EmailField()
    class Meta:
        model = User
        fields = ['username',
            'email',
            'first_name',
            'last_name',
        ]

class PerfilUpdateForm(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = ['rut']

from django.contrib import admin
from .models import Perfil, Anuncio, Departamento, Visita, Visitante
# Register your models here.

@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
    pass

@admin.register(Anuncio)
class AnuncioAdmin(admin.ModelAdmin):
    pass

@admin.register(Departamento)
class DepartamentoAdmin(admin.ModelAdmin):
    pass

@admin.register(Visita)
class VisitaAdmin(admin.ModelAdmin):
    pass

@admin.register(Visitante)
class VisitanteAdmin(admin.ModelAdmin):
    pass

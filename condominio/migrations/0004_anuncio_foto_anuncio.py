# Generated by Django 3.0.4 on 2020-07-07 03:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('condominio', '0003_anuncio_fecha'),
    ]

    operations = [
        migrations.AddField(
            model_name='anuncio',
            name='foto_anuncio',
            field=models.ImageField(blank=True, null=True, upload_to='fotos/anuncio', verbose_name='Foto del anuncio'),
        ),
    ]

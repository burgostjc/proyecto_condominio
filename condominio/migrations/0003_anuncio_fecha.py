# Generated by Django 3.0.4 on 2020-06-27 10:14

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('condominio', '0002_auto_20200627_1013'),
    ]

    operations = [
        migrations.AddField(
            model_name='anuncio',
            name='fecha',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]

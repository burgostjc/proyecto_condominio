from django.shortcuts import render, redirect
from condominio.models import Perfil, Anuncio, Departamento, Visitante, Visita
from django.contrib.auth.models import User #, Group
from django.contrib.auth.decorators import login_required, permission_required #, user_passes_test
from condominio.forms import (VisitanteForm,
        VisitaForm,
        DepartamentoForm,
        AnuncioForm,
        PerfilForm,
        UserUpdateForm,
        PerfilUpdateForm
    )

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.core.exceptions import ObjectDoesNotExist

from django.contrib import messages

from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash



def home(request):
    template_name = 'condominio/home.html'
    data = {}

    data['query'] = ''

    return render(request, template_name, data)


##############################################################
##########################VISITANTE###########################
##############################################################
@permission_required('condominio.view_visitante')
def lista_visitante(request):
	template_name = 'condominio/visitante.html'
	data = {}

	data['visitantes'] = Visitante.objects.all()

	return render(request, template_name, data)

@permission_required('condominio.add_visitante')
def crear_visitante(request):
	data = {}
	template_name = 'condominio/crear_visitante.html'

	if request.method == 'POST':
		data['form'] = VisitanteForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('condominio:lista_visitante')
	else:
		data['form'] = VisitanteForm()

	return render(request, template_name, data)

@permission_required('condominio.change_visitante')
def editar_visitante(request, visitante_id):
	data = {}
	template_name = 'condominio/editar_visitante.html'
	visitante = Visitante.objects.get(pk=visitante_id)

	if request.method == 'POST':
		data['form'] = VisitanteForm(request.POST,request.FILES or None, instance=visitante)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('condominio:lista_visitante')
	else:
		data['form'] = VisitanteForm(instance=visitante)

	return render(request, template_name, data)

@permission_required('condominio.delete_visitante')
def eliminar_visitante(request, visitante_id):
	data = {}
	template_name = 'condominio/eliminar_visitante.html'
	data['visitante'] = Visitante.objects.get(pk=visitante_id)

	if request.method == 'POST':
		data['visitante'].delete()
		return redirect('condominio:lista_visitante')

	return render(request, template_name, data)



##############################################################
############################VISITA############################
##############################################################
@permission_required('condominio.view_visita')
def lista_visita(request): #talvez agregar paginador despues de ver que tenemos todo lo necesario en la vista
	template_name = 'condominio/visita.html'
	data = {}

	data['visitas'] = Visita.objects.all()

	return render(request, template_name, data)

@permission_required('condominio.add_visita')
def crear_visita(request):
	data = {}
	template_name = 'condominio/crear_visita.html'

	if request.method == 'POST':
		data['form'] = VisitaForm(request.POST)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('condominio:lista_visita')
	else:
		data['form'] = VisitaForm()

	return render(request, template_name, data)

@permission_required('condominio.change_visita')
def editar_visita(request, visita_id):
	data = {}
	template_name = 'condominio/editar_visita.html'
	visita = Visita.objects.get(pk=visita_id)

	if request.method == 'POST':
		data['form'] = VisitaForm(request.POST,request.FILES or None, instance=visita)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('condominio:lista_visita')
	else:
		data['form'] = VisitaForm(instance=visita)

	return render(request, template_name, data)

@permission_required('condominio.delete_visita')
def eliminar_visita(request, visita_id):
	data = {}
	template_name = 'condominio/eliminar_visita.html'
	data['visita'] = Visita.objects.get(pk=visita_id)

	if request.method == 'POST':
		data['visita'].delete()
		return redirect('condominio:lista_visita')

	return render(request, template_name, data)



##############################################################
##########################DEPARTAMENTO########################
##############################################################
@permission_required('condominio.view_departamento')
def lista_departamento(request):
	template_name = 'condominio/departamento.html'
	data = {}

	data['departamentos'] = Departamento.objects.all()

	return render(request, template_name, data)

@permission_required('condominio.add_departamento')
def crear_departamento(request):
	data = {}
	template_name = 'condominio/crear_departamento.html'

	if request.method == 'POST':
		data['form'] = DepartamentoForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('condominio:lista_departamento')
	else:
		data['form'] = DepartamentoForm()

	return render(request, template_name, data)

@permission_required('condominio.change_departamento')
def editar_departamento(request, departamento_id):
	data = {}
	template_name = 'condominio/editar_departamento.html'
	departamento = Departamento.objects.get(pk=departamento_id)

	if request.method == 'POST':
		data['form'] = DepartamentoForm(request.POST,request.FILES or None, instance=departamento)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('condominio:lista_departamento')
	else:
		data['form'] = DepartamentoForm(instance=departamento)

	return render(request, template_name, data)

@permission_required('condominio.delete_departamento')
def eliminar_departamento(request, departamento_id):
	data = {}
	template_name = 'condominio/eliminar_departamento.html'
	data['departamento'] = Departamento.objects.get(pk=departamento_id)

	if request.method == 'POST':
		data['departamento'].delete()
		return redirect('condominio:lista_departamento')

	return render(request, template_name, data)


##############################################################
###########################ANUNCIOS###########################
##############################################################
@permission_required('condominio.view_anuncio')
def verAnunciosViews(request):
    template_name = 'condominio/anuncios.html'
    data = {}

    # anuncios_list = Anuncio.objects.all().order_by('fecha').reverse()

    #########ANUNCIOS DE LOS ADMIN###############
    page = request.GET.get('page', 1)
    anuncios_list = Anuncio.objects.filter(perfil__user__groups__name__in = ['administrador']).order_by('fecha').reverse()

    paginator = Paginator(anuncios_list, 2)

    try:
        data['anunciosAdmin'] = paginator.page(page)
    except PageNotAnInteger:
        data['anunciosAdmin'] = paginator.page(1)
    except EmptyPage:
        data['anunciosAdmin'] = paginator.page(paginator.num_pages)


    #########ANUNCIOS DE LOS CONSERJES###############
    page2 = request.GET.get('page2', 1)
    anuncios_list2 = Anuncio.objects.filter(perfil__user__groups__name__in = ['conserje']).order_by('fecha').reverse()

    paginator2 = Paginator(anuncios_list2, 2)

    try:
        data['anunciosConserje'] = paginator2.page(page2)
    except PageNotAnInteger:
        data['anunciosConserje'] = paginator2.page(1)
    except EmptyPage:
        data['anunciosConserje'] = paginator2.page(paginator2.num_pages)


    #########ANUNCIOS DE LOS USUARIOS###############
    page3 = request.GET.get('page3', 1)
    anuncios_list3 = Anuncio.objects.filter(perfil__user__groups__name__in = ['habitante']).order_by('fecha').reverse()

    paginator3 = Paginator(anuncios_list3, 5)

    try:
        data['anunciosUser'] = paginator3.page(page3)
    except PageNotAnInteger:
        data['anunciosUser'] = paginator3.page(1)
    except EmptyPage:
        data['anunciosUser'] = paginator3.page(paginator3.num_pages)

    return render(request, template_name, data)

@permission_required('condominio.add_anuncio')
def crearAnunciosViews(request):
    template_name = 'condominio/crear_anuncio.html'
    data = {}

    data['form'] = AnuncioForm(request.POST or None, request.FILES or None)
    try:

        if data['form'].is_valid():
            user_profile = Perfil.objects.get(user=request.user)
            if user_profile:
                instance = data['form'].save(commit=False)
                instance.perfil = user_profile
                instance.save()
                return redirect('condominio:anuncios')
        else:

            data['form'] = AnuncioForm(instance=request.user)
    except ObjectDoesNotExist:
        messages.error(
            request,
            'Este Usuario no tiene un perfil creado'
        )
        data['form'] = AnuncioForm(instance=request.user)
        # return redirect('condominio:anuncios')


    return render(request, template_name, data)

@permission_required('condominio.delete_anuncio')
def borrarAnuncioViews(request, anuncio_id):

    data = {}
    template_name = 'condominio/eliminar_anuncio.html'

    data['anuncio'] = Anuncio.objects.get(id=anuncio_id)

    if data['anuncio'].perfil.user == request.user:
        if request.method == 'POST':
            data['anuncio'].delete()
            return redirect('condominio:anuncios')
    else:
        return HttpResponse('No estás autorizado para ver esta página.')


    return render(request, template_name, data)


from django.http import HttpResponse
# @check_profile
@permission_required('condominio.change_anuncio')
def editarAnuncioViews(request, anuncio_id):

    data = {}
    template_name = 'condominio/editar_anuncio.html'

    anuncio = Anuncio.objects.get(pk=anuncio_id)

    if anuncio.perfil.user == request.user:

        if request.method == 'POST':
            form = AnuncioForm(request.POST, request.FILES or None, instance=anuncio)
            if form.is_valid():
                form.save()
                return redirect('condominio:anuncios')


        else:
            data['form'] = AnuncioForm(instance=anuncio)

    else:
        return HttpResponse('No estás autorizado para ver esta página.')

    return render(request, template_name, data)


##############################################################
###########################USUARIOS###########################
##############################################################
@permission_required('condominio.view_perfil')
def verUsuariosViews(request):
    template_name = 'condominio/usuarios.html'
    data = {}

    page = request.GET.get('page', 1)
    perfiles_list = Perfil.objects.all().order_by('user__date_joined').reverse()

    paginator = Paginator(perfiles_list, 10)

    try:
        data['perfiles'] = paginator.page(page)
    except PageNotAnInteger:
        data['perfiles'] = paginator.page(1)
    except EmptyPage:
        data['perfiles'] = paginator.page(paginator.num_pages)

    return render(request, template_name, data)

@permission_required('condominio.change_perfil')
def editarUsuariosViews(request, perfil_id):
    data = {}
    template_name = 'condominio/editar_usuario.html'

    try:
        perfil = Perfil.objects.get(pk=perfil_id)

        if request.method == 'POST':
            user_form = UserUpdateForm(request.POST, instance=perfil.user)
            perfil_form = PerfilUpdateForm(request.POST, instance=perfil)
            if user_form.is_valid() and perfil_form.is_valid():
                user_form.save()
                perfil_form.save()
                return redirect('condominio:usuarios')


        else:
            user_form = UserUpdateForm(instance=perfil.user)
            perfil_form = PerfilUpdateForm(instance=perfil)
    except ObjectDoesNotExist:
        messages.error(
            request,
            'Este Usuario no tiene un perfil creado'
        )
        user_form = UserUpdateForm(instance=perfil.user)
        perfil_form = PerfilUpdateForm(instance=perfil)

    data = {
        'user_form': user_form,
        'perfil_form': perfil_form

    }

    return render(request, template_name, data)

@permission_required('condominio.delete_perfil')
def borrarUsuarioViews(request, usuario_id):

    data = {}
    template_name = 'condominio/eliminar_usuario.html'

    data['usuario'] = User.objects.get(id=usuario_id)
    if request.method == 'POST':
        data['usuario'].delete()
        return redirect('condominio:usuarios')

    return render(request, template_name, data)


##############################################################
###########################PERFIL#############################
##############################################################
@login_required
def verPerfilViews(request):
    template_name = 'condominio/perfil.html'
    data = {}

    # perfil = Perfil.objects.get(user=request.user)

    return render(request, template_name, data)

@login_required
def actualizarPassPerfil(request):

    template_name = 'condominio/actualizarPassProfile.html'
    data = {}

    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('condominio:perfil')

    else:
        form = PasswordChangeForm(user=request.user)

    data = {
        'form': form,

    }

    return render(request, template_name, data)

# def lista_anuncio(request):
# 	template_name = 'condominio/visitante.html'
# 	data = {}

# 	data['visitantes'] = Visitante.objects.all()

# 	return render(request, template_name, data)


#falta view de perfil (talvez ponerlo en el home) ¿¿view de conserje y administrativos???

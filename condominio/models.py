from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Perfil(models.Model):
    rut = models.CharField('Rut', max_length=20, null=False, blank=False)
    foto_perfil = models.ImageField('Foto de perfil',
        upload_to='fotos/perfil',
        blank=True,
        null=True
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'Perfil'
        verbose_name_plural = 'Perfiles'

    def __str__(self):
        return str(self.user)


class Anuncio(models.Model):
    titulo = models.CharField('Titulo', max_length=100, null=False, blank=False)
    descripcion = models.TextField('Descripción', max_length=500, null=False, blank=False)
    fecha = models.DateTimeField(auto_now_add=True)
    foto_anuncio = models.ImageField('Foto del anuncio',
        upload_to='fotos/anuncio',
        blank=True,
        null=True
    )

    perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE, null=False, blank=False)

    class Meta:
        ordering = ['titulo']
        verbose_name = 'Anuncio'
        verbose_name_plural = 'Anuncios'

    def __str__(self):
        return self.titulo


class Departamento(models.Model):
    numero = models.CharField('N° Departamento', max_length=15, null=False, blank=False)

    perfil = models.ManyToManyField(Perfil, blank=True, related_name='depto_perfil')

    class Meta:
        ordering = ['numero']
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'

    def __str__(self):
        return self.numero


class Visitante(models.Model):
    nombre = models.CharField('Nombre', max_length=50, null=False, blank=False)
    apellido = models.CharField('Apellido', max_length=50, null=True, blank=True)
    rut = models.CharField('Rut', max_length=20, null=True, blank=True)

    class Meta:
        ordering = ['apellido']
        verbose_name = 'Visitante'
        verbose_name_plural = 'Visitantes'

    def __str__(self):
        return self.nombre


class Visita(models.Model):
    observacion = models.TextField(max_length=500, null=True, blank=True)
    fecha = models.DateTimeField(auto_now_add=True)

    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE, null=False, blank=False)
    visitante = models.ManyToManyField(Visitante, related_name='visitante')

    class Meta:
        ordering = ['id']
        verbose_name = 'Visita'
        verbose_name_plural = 'Visitas'

    def __str__(self):
        return self.departamento.numero
        # return str(self.departamento.numero)
